import shutil, os, sys, subprocess, re, fnmatch, io, json
from datetime import date, datetime
from ftplib import FTP
        

currentDirectory = os.getcwd()
save_directory = os.path.join(currentDirectory, 'cleaner.json')
print ("FTPCleaner starting..........")

def download():
    try:
        ftp = FTP('ftp.sunbeltsoftware.com', 'phftp', 'm0reb!gFil3s')
        ftp.cwd('/FarrelJ/whiteEMLconfig')
        files = ftp.nlst()
        filename = files[0] # get first file

        ftp.retrbinary("RETR " + filename, open(save_directory, 'wb').write)
        ftp.close()

        return True
    except Exception as e:
        print(e)
        return False

if download():
   with open(save_directory, 'r') as j:
    json_data = json.load(j)
    pathremove = json_data['FilesToRemoved']
    for dataproc in pathremove:
        splitdata = str(dataproc).split(", ")
        pathremovebracket = splitdata[0].replace('{', '')
        paths = pathremovebracket.split(", ")
        networkconnect = 'ftp.sunbeltsoftware.com'
        usern = 'phftp'
        pwconnect = 'm0reb!gFil3s'
        for path in paths:
            try:
                ftp = FTP(networkconnect, usern, pwconnect)
            except Exception as y:
                print(y)
            else:
                if re.search(r"path_", path):
                    getdata = path.split(": ")
                    getdata1 = str(getdata[1])
                    getdata2 = getdata1.replace('\'', '')
                    ftp.cwd(getdata2)
                    pathcontent = ftp.nlst()
                    for file in pathcontent:
                        filetoprocess = str(file)
                        combinepath = str(getdata2) + "/" + filetoprocess
                        #print(combinepath)
                        remote_datetime = ftp.voidcmd("MDTM " + combinepath)[4:].strip()
                        dateToday = date.today()
                        filetime = str(remote_datetime)
                        filetime1 = filetime[:8]
                        filetime2 = filetime1[:4] + '-' + filetime1[4] + filetime1[5] + '-' + filetime1[6] + filetime1[7]
                        #print(dateToday) 
                        #print(filetime2)    
                        date_format = "%Y-%m-%d"
                        a = datetime.strptime(str(dateToday), date_format)
                        b = datetime.strptime(str(filetime2), date_format)
                        delta = a - b
                        datedifference = int(delta.days)
                        filethreshold = splitdata[1].replace('}', '')
                        thisisthreshold = filethreshold.split(": ")
                        finalthreshold = str(thisisthreshold[1]).replace('\'', '')
                        if datedifference >= int(finalthreshold):
                            print ("File " + filetoprocess + " is longer than the set threshold " + str(finalthreshold) + " and will now be removed.")
                            ftp.delete(filetoprocess)
                        else:
                            continue
                else:
                    continue
        ftp.close()